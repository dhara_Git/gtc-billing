package com.gtc.services;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtc.daos.UserDao;
import com.gtc.model.Product;
import com.gtc.model.Users;

//import com.gtc.dao.UserDao;




@Service("userService")
@Transactional
public class UserServiceImpl implements UserService
{
	@Autowired
	private UserDao userDao;
	 public String authUser(String userId,String password)
	{
		return userDao.authUser(userId,password);
	}
    public void insertOrUpdateUser(Users user)
	{
		userDao.insertOrUpdateUser(user);		
	}
    public void insertOrUpdateBill(Product p) {
		userDao.insertOrUpdateBill(p);
		
	}

	
	
}