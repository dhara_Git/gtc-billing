package com.gtc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.gtc.dao.ProductDao;
import com.gtc.model.Product;
import com.gtc.services.UserService;
import com.gtc.model.Users;

@Controller
public class MainController {

	@Autowired
	private ProductDao productDao;

	
	@Autowired
	UserService userService;
	@RequestMapping("/")
	public String home(Model m) {

		List<Product> products = productDao.getProducts();
		m.addAttribute("products", products);

		return "home";
	}

	// show add product form
	@RequestMapping("/add-product")
	public String addProduct(Model m) {
		m.addAttribute("title", "Add Product");
		return "add_product_form";
	}

	@RequestMapping("/login")
	public String auth(Model m,HttpServletRequest req) {

		List<Product> products = productDao.getProducts();
		m.addAttribute("products", products);

		return "home";
	}
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String authUser(ModelMap model,HttpServletRequest request,@RequestParam("userId")String userId,@RequestParam("password")String password)
	{
		System.out.println("1");
		List<Product> products = productDao.getProducts();
		System.out.println("2");
		model.addAttribute("products", products);
		System.out.println("3");
		//productDao.createProduct(product);
		String flage=userService.authUser(userId,password);
		System.out.println("4");
		if(flage.equals("Success"))
		{
			HttpSession session=request.getSession();
			//session.setAttribute("uBean",userService.getUserBean(userId));
			//commonService.insertLogs(userId,"Logged In");
			return "redirect:/home";
		}
		else
		{
			if(flage.equals("Disabled"))
				model.addAttribute("msg","Your UserId has been disabled<br>Contact your Administrator");
			else
				model.addAttribute("msg","Wrong UserId/Password");
			return "index";
		}
		//return "";
	}
	
	@RequestMapping(value="/saveBill",method=RequestMethod.POST)
	public String addUser(HttpServletRequest request,@ModelAttribute("billForm")Product product,Model m) {
		List<Product> products = productDao.getProducts();
		m.addAttribute("products", products);
		productDao.createProduct(product);
		return "success";

	}
	@RequestMapping(value="/saveAdmin",method=RequestMethod.POST)
	public String addPermisssions(HttpServletRequest request,@ModelAttribute("billForm")Users user,Model m) {
		/*List<Product> products = productDao.getProducts();
		m.addAttribute("products", products);
		productDao.createProduct(product);*/
		userService.insertOrUpdateUser(user);
		return "home";

	}
	@RequestMapping(value="/home", method = RequestMethod.GET)
    public String home(HttpServletRequest req,Model m) {
		List<Product> products = productDao.getProducts();
		m.addAttribute("products", products);
		System.out.println("1");
    	return "home";
    }
	
	
	
	

	
	@RequestMapping(value = "/updateBill", method = RequestMethod.POST)
	public RedirectView updateBill(@ModelAttribute("billForm")Product product, HttpServletRequest request) {
		System.out.println(product);
		productDao.createProduct(product);
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl(request.getContextPath() + "/");
		return redirectView;
	}
	
	// delete handlerxx
	@RequestMapping(value="/delete/{productId}",method=RequestMethod.GET)
	public String deleteProduct(@PathVariable("productId") int productId, HttpServletRequest request,Model m) {
		this.productDao.deleteProduct(productId);

		List<Product> products = productDao.getProducts();
		System.out.println(products);
		m.addAttribute("products", products);

		return "redirect:/home";}
	
	
	///@RequestMapping(value="/updateBill")
	public String updateForm1(@PathVariable("id") int id,@ModelAttribute("billForm")Product p,Model model,HttpServletRequest req)
	{    System.out.println("1001");
		Product product = this.productDao.getProduct(id);
		System.out.println("1011");
		model.addAttribute("billForm", p);
		System.out.println("Product:"+product);
		//userService.insertOrUpdateUser(p);
		productDao.createProduct(product);
		//HttpSession session=req.getSession(false);
				//session.setAttribute("model", model);
		return "home";
	}
	@RequestMapping(value="/update/{productId}")
	public String updateForm(@PathVariable("productId") int pid,@ModelAttribute("billForm")Product p,Model model,HttpServletRequest req)
	{    System.out.println("100");
		Product product = this.productDao.getProduct(pid);
		System.out.println("101");
		model.addAttribute("billForm", product);
		System.out.println("Product:"+product);
		//HttpSession session=req.getSession(false);
				//session.setAttribute("model", model);
		return "update";
	}
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logout(HttpServletRequest req) {
		HttpSession session=req.getSession(false);
		session.invalidate();
		return "login";
		
	}
}
