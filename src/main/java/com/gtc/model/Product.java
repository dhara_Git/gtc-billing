package com.gtc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String vehiclenum;
	private  String vehiclename;
	private String productname;
    private int	totalweight;
	private int emptyweight;
	private int productweight;
	private long paidrupees;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVehiclenum() {
		return vehiclenum;
	}
	public void setVehiclenum(String vehiclenum) {
		this.vehiclenum = vehiclenum;
	}
	public String getVehiclename() {
		return vehiclename;
	}
	public void setVehiclename(String vehiclename) {
		this.vehiclename = vehiclename;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public int getTotalweight() {
		return totalweight;
	}
	public void setTotalweight(int totalweight) {
		this.totalweight = totalweight;
	}
	public int getEmptyweight() {
		return emptyweight;
	}
	public void setEmptyweight(int emptyweight) {
		this.emptyweight = emptyweight;
	}
	public int getProductweight() {
		return productweight;
	}
	public void setProductweight(int productweight) {
		this.productweight = productweight;
	}
	public long getPaidrupees() {
		return paidrupees;
	}
	public void setPaidrupees(long paidrupees) {
		this.paidrupees = paidrupees;
	}
	public Product(int id, String vehiclenum, String vehiclename, String productname, int totalweight, int emptyweight,
			int productweight, long paidrupees) {
		super();
		this.id = id;
		this.vehiclenum = vehiclenum;
		this.vehiclename = vehiclename;
		this.productname = productname;
		this.totalweight = totalweight;
		this.emptyweight = emptyweight;
		this.productweight = productweight;
		this.paidrupees = paidrupees;
	}
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", vehiclenum=" + vehiclenum + ", vehiclename=" + vehiclename + ", productname="
				+ productname + ", totalweight=" + totalweight + ", emptyweight=" + emptyweight + ", productweight="
				+ productweight + ", paidrupees=" + paidrupees + "]";
	}
	
}