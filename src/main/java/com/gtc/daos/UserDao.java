package com.gtc.daos;

import java.util.ArrayList;

import com.gtc.model.Product;
import com.gtc.model.Users;



public interface UserDao
{
	public String authUser(String userId,String password);
	public void insertOrUpdateUser(Users user);
	public void insertOrUpdateBill(Product p);
}
