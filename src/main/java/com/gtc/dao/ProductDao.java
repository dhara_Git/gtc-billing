package com.gtc.dao;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.gtc.model.Product;

@Component
public class ProductDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	// create
	@Transactional
	public void createProduct(Product product) {
		
	

		this.hibernateTemplate.saveOrUpdate(product);

	}

	// get all products
	public List<Product> getProducts() {
		List<Product> products = this.hibernateTemplate.loadAll(Product.class);
		return products;
	}

	// delete the single product
	@Transactional
	public void deleteProduct(int pid) {
		Product p = this.hibernateTemplate.load(Product.class, pid);
		this.hibernateTemplate.delete(p);
	}

	// get the single product
	public Product getProduct(int pid) {
		return this.hibernateTemplate.get(Product.class, pid);
	}
	@Transactional
	public int getbillid() {
String	hql ="select max(product.id) from Product product";


Query   Query=  hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hql);
 Object id = Query.getSingleResult();
int Id= (Integer) id;
System.out.println(Id);
//int id= Integer.parseInt(ID);
Id++;
		return Id;
	}

	

	public List<Bill> getbilldata() {
		List<Bill> bill = this.hibernateTemplate.loadAll(Bill.class);
		return bill;
	}



	

	public void createBilldata(List<Bill> billdata) {
		this.hibernateTemplate.saveOrUpdate(billdata);
		
	}
}
