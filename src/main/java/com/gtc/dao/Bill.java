package com.gtc.dao;

public class Bill {
	
	private int invoiceno;private String invoicedate;private String road;private int vehicleno;
	private String dateofsupply;private String placeofsupply;private String name;private int mobno;
	private String address;private String state;private int code;private String nameofproduct;
	private int hsn;private int  qty;private int weight;private int rate;private int amount;
	private int ttlbtax;@Override
	public String toString() {
		return "Bill [invoiceno=" + invoiceno + ", invoicedate=" + invoicedate + ", road=" + road + ", vehicleno="
				+ vehicleno + ", dateofsupply=" + dateofsupply + ", placeofsupply=" + placeofsupply + ", name=" + name
				+ ", mobno=" + mobno + ", address=" + address + ", state=" + state + ", code=" + code
				+ ", nameofproduct=" + nameofproduct + ", hsn=" + hsn + ", qty=" + qty + ", weight=" + weight
				+ ", rate=" + rate + ", amount=" + amount + ", ttlbtax=" + ttlbtax + ", totalamt=" + totalamt
				+ ", taxtype=" + taxtype + "]";
	}


	public Bill() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Bill(int invoiceno, String invoicedate, String road, int vehicleno, String dateofsupply,
			String placeofsupply, String name, int mobno, String address, String state, int code, String nameofproduct,
			int hsn, int qty, int weight, int rate, int amount, int ttlbtax, int totalamt, int taxtype) {
		super();
		this.invoiceno = invoiceno;
		this.invoicedate = invoicedate;
		this.road = road;
		this.vehicleno = vehicleno;
		this.dateofsupply = dateofsupply;
		this.placeofsupply = placeofsupply;
		this.name = name;
		this.mobno = mobno;
		this.address = address;
		this.state = state;
		this.code = code;
		this.nameofproduct = nameofproduct;
		this.hsn = hsn;
		this.qty = qty;
		this.weight = weight;
		this.rate = rate;
		this.amount = amount;
		this.ttlbtax = ttlbtax;
		this.totalamt = totalamt;
		this.taxtype = taxtype;
	}


	public int getInvoiceno() {
		return invoiceno;
	}


	public void setInvoiceno(int invoiceno) {
		this.invoiceno = invoiceno;
	}


	public String getInvoicedate() {
		return invoicedate;
	}


	public void setInvoicedate(String invoicedate) {
		this.invoicedate = invoicedate;
	}


	public String getRoad() {
		return road;
	}


	public void setRoad(String road) {
		this.road = road;
	}


	public int getVehicleno() {
		return vehicleno;
	}


	public void setVehicleno(int vehicleno) {
		this.vehicleno = vehicleno;
	}


	public String getDateofsupply() {
		return dateofsupply;
	}


	public void setDateofsupply(String dateofsupply) {
		this.dateofsupply = dateofsupply;
	}


	public String getPlaceofsupply() {
		return placeofsupply;
	}


	public void setPlaceofsupply(String placeofsupply) {
		this.placeofsupply = placeofsupply;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getMobno() {
		return mobno;
	}


	public void setMobno(int mobno) {
		this.mobno = mobno;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public String getNameofproduct() {
		return nameofproduct;
	}


	public void setNameofproduct(String nameofproduct) {
		this.nameofproduct = nameofproduct;
	}


	public int getHsn() {
		return hsn;
	}


	public void setHsn(int hsn) {
		this.hsn = hsn;
	}


	public int getQty() {
		return qty;
	}


	public void setQty(int qty) {
		this.qty = qty;
	}


	public int getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}


	public int getRate() {
		return rate;
	}


	public void setRate(int rate) {
		this.rate = rate;
	}


	public int getAmount() {
		return amount;
	}


	public void setAmount(int amount) {
		this.amount = amount;
	}


	public int getTtlbtax() {
		return ttlbtax;
	}


	public void setTtlbtax(int ttlbtax) {
		this.ttlbtax = ttlbtax;
	}


	public int getTotalamt() {
		return totalamt;
	}


	public void setTotalamt(int totalamt) {
		this.totalamt = totalamt;
	}


	public int getTaxtype() {
		return taxtype;
	}


	public void setTaxtype(int taxtype) {
		this.taxtype = taxtype;
	}


	private int totalamt;private int taxtype;
	

}
