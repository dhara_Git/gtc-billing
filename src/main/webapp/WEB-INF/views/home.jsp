<html>
<head>
<%@include file="./base.jsp"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="billForm"%>

</head>
 


<body>
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="bill.jsp">Bill</a>
  <a href="abc.jsp">Bilty</a>
  <a href="@Url.Action("kantabill", "kantabill")">kantabill</a><br />
<a href="<c:url value="/kantabill" />" >kantabillurl</a>
  <a href="admin.jsp">Administrator</a>
  <a href="contact.jsp">Contact</a>
  <a href="logs.jsp">Logs</a>
  <h3><a href="${pageContext.request.contextPath}/logout">LogOut</a></h3>
  
</div>
<div id="logout" class="logout"> <h3><a href="${pageContext.request.contextPath}/logout">LogOut</a></h3></div>


<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>

	<div class="container mt-3">

		<div class="row">

			<div class="col-md-12">
<billForm:form>
				<h1 class="text-center mb-3">Welcome to GTC Billing System</h1>

				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Id</th>
							<th scope="col">Product Name</th>
							<th scope="col">Vehicle name</th>
								<th scope="col">Vehicle No.</th>
							<th scope="col">Productweight</th>
							<th scope="col">Emptyweight</th>
							<th scope="col">Totalweight</th>
							<th scope="col">Paid Rupees</th>
							<th scope="col">Delete/Update</th>
							
						</tr>			
					</thead>
					<tbody>
	
						<c:forEach items="${products }" var="p">
							<tr>
								<th scope="row">Bill ${p.id }</th>
								<td>${p.productname }</td>
								<td>${p.vehiclename }</td>	
								<td>${p.vehiclenum}</td>								
								<td>${p.productweight }</td>
								<td>${p.emptyweight }</td>
								<td>${p.totalweight }</td>
								<td class="font-weight-bold">&#x20B9; ${p.paidrupees }</td>
								<td>
								<a href="delete/${p.id }"><i class="fas fa-trash text-danger" style="font-size: 30px;"></i></a>
								<a href="update/${p.id }"><i class="fas fa-pen-nib text-primary" style="font-size: 30px;"></i></a>
							
								
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

				<div class="container text-center">

					<a href="add-product" class="btn btn-outline-success">print</a>

				</div>


			</div>
</billForm:form>

		</div>

	</div>
</body>
<style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
.logout{ text-align:center; color: black;
width: 65px; height:90px;

}
div.logout{
position : absolute; 
left:1200px ;
top : 10px;
}
</style>

</html>
