

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="billForm"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
/* function updateUser()
{alert("alert box!");
	/* mscConfirm("Are you sure, you want to update these details?",function(){
		document.getElementById('updateBillForm').submit();
	}); */
/* document.getElementById('updateBillForm').submit();
} */

/* function updateUser()
{      alert("alert box!"); */
	/* mscConfirm("Are you sure, you want to remove this user?",function(){
  		window.location="updateBill?productId="+document.getElementById('productId').value;
	}); */
/* window.location="updateBill?id="+document.getElementById('id').value; */

/* window.location="updateBill?id="+document.getElementById('id').value; */

</script>



<div class="container">
	<p class="h1" style="font-family: cambria math; text-align: center; color: #387403;">Update Details</p><br>
	<billForm:form action="updateBill" id="updateBillForm" method="post" modelAttribute="billForm">
        <table class="table">
         <tr>
             
                <td align="center"><label style="color:black; font-family: cambria;" for="id"><h4><b>id</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="id" id="id" required="true"  autocomplete="off" readonly="true"/><   /td>
            </tr>
            <tr>
              <a href="<c:url value="/home"/>" >Home</a>
                <td align="center"><label style="color:black; font-family: cambria;" for="vehiclenum"><h4><b>vehiclenum</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="vehiclenum" id="vehiclenum" required="true"/></td>
            </tr>
            <tr>
              	<td align="center"><label style="color:black; font-family: cambria;" for="productname"><h4><b>productname:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="productname" id="productname" required="true"/></td>
                
            </tr>
            <tr>
                <td align="center"><label style="color:black; font-family: cambria;" for="totalweight"><h4><b>totalweight:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="totalweight" id="totalweight" required="true"/></td>
               </tr> 
            <tr>  
                <td align="center"><label style="color: black; font-family: cambria;" for="emptyweight"><h4><b>emptyweight</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="emptyweight" id="emptyweight" required="true"/></td>
            </tr>
            <tr>
                <td align="center"><label style="color:black; font-family: cambria;" for="paidrupees"><h4><b>paidrupees:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="paidrupees" id="paidrupees" required="true"/></td>
                
            </tr>
            <tr>
            	<td align="center"><label style="color: black; font-family: cambria;" for="productweight"><h4><b>productweight:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="productweight" id="productweight" required="true"/></td>
                     
            </tr>
              <tr>
            	<td colspan="2" align="center"><button type="submit" class="btn btn-warning">Update</button></td>
            	
           	</tr>  
                     
           
        </table>
        <billForm:hidden path="id"/>
    </billForm:form>
</div>