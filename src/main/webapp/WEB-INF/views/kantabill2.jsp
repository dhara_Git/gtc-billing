

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="userForm"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="<c:url value='/staticResources/scripts/users.js'/>"></script>

<script type="text/javascript">
	
</script>





<div style="margin-bottom: 0px; padding-bottom: 0px;" class="container">
    <p class="h2" style="color: #387403; font-family: cambria; text-align: center"><b><i>Add New User</i></b></p>
    <userForm:form action="handle-product"  method="post" modelAttribute="userForm">
        <table class="table">
            <tr>
                <td align="center"><label style="color:black; font-family: cambria;" for="vehiclenum"><h4><b>vehiclenum:</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" id="vehiclenum" path="vehiclenum" autocomplete="off" required="true"/></td>
                <td align="center"><label style="color:black; font-family: cambria;" for="vehicle"><h4><b>vehicle:</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" path="vehicle" id="vehicle" required="true"/></td>
            </tr>
            <tr>
              	<td align="center"><label style="color:black; font-family: cambria;" for="productname."><h4><b>productname:</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" path="productname" id="productname" required="true"/></td>
                <td align="center"><label style="color:black; font-family: cambria;" for="totalweight"><h4><b>totalweight</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" path="totalweight" id="totalweight" required="true"/></td>
            </tr>
            <tr>
                <td align="center"><label style="color:black; font-family: cambria;" for="emptyweight"><h4><b>emptyweight:</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" path="emptyweight" id="emptyweight" required="true"/></td>
                <td align="center"><label style="color: black; font-family: cambria;" for="productweight"><h4><b>productweight:</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" path="lastName" id="productweight" required="true"/></td>
            </tr>
            <tr>
            	<td align="center"><label style="color: black; font-family: cambria;" for="paidrupees"><h4><b>paidrupees</b></h4></label></td>
                <td><userForm:input style="width: 250px; height: 35px;" path="employeeId" id="paidrupees" required="true"/></td>
                
                
            </tr>
            <tr>
            	
            </tr>
            
            <tr><td colspan="4" align="center"><input class="btn btn-primary" style="background-color: #387403; color: #ffffff;" value="Add User" onclick="addUser();"></td></tr>
        </table>
    </userForm:form>
</div>