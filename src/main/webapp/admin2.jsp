<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="billForm"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div>
    <p class="h2" style="color: #387403; font-family: cambria; text-align: center"><b><i>Add New User</i></b></p>
   
    <billForm:form>
        <table class="table">
            <tr>
                <td align="center"><label style="color:black; font-family: cambria;" for="User Id"><h4><b>User Id:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" id="userId" path="userId" autocomplete="off" required="true"/></td>
                <td align="center"><label style="color:black; font-family: cambria;" for="Password"><h4><b>Password:</b></h4></label></td>
                <td><billForm:password style="width: 250px; height: 35px;" path="password" id="password" required="true"/></td>
            </tr>
            
            <tr>
                <td align="center"><label style="color:black; font-family: cambria;" for="First Name"><h4><b>First Name:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="firstName" id="firstName" required="true"/></td>
                <td align="center"><label style="color: black; font-family: cambria;" for="Last Name"><h4><b>Last Name:</b></h4></label></td>
                <td><billForm:input style="width: 250px; height: 35px;" path="lastName" id="lastName" required="true"/></td>
            </tr>
            <tr>
            	
                <td align="center"><label style="color: black; font-family: cambria;" for="User Type"><h4><b>User Type:</b></h4></label></td>
                <td>
                    <billForm:select class="form-control" style="margin-top: 6px; height: 32px; width: 152px;" path="userType" id="userType" required="true" onchange="setDepartment();">
                  		<billForm:option class="form-control" value="Select" label="Select"/>
                       	<billForm:option class="form-control" value="Super Administrator" label="Super Administrator"/>
                       	<billForm:option class="form-control" value="Administrator" label="Administrator"/>
                       	<billForm:option class="form-control" value="User" label="User"/>
                   </billForm:select>
                </td>
            </tr>
            
            <tr>
                <td colspan="4" align="center">
                  	<label style="color:black; font-family: cambria;" for="Permissions"><h4><b>Select Permissions:</b></h4></label>
                  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  	                     		
                   		<billForm:checkbox style="margin-top: 12px;" value="true" path="print" id="print"/>Print Files&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   		<billForm:checkbox style="margin-top: 12px;" value="true" path="update" id="update"/>Update Files&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   		
                   		
                   		<billForm:checkbox style="margin-top: 12px;" value="true" path="logs" id="logs"/>Logs
               </td>
            </tr>
            <tr><td colspan="4" align="center"><input class="btn btn-primary" style="background-color: #387403; color: #ffffff;" value="Add User" onclick="addUser();"></td></tr>
        </table>
    </billForm:form>
</div>
</body>
</html>